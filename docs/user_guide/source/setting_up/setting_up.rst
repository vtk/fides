###################
Install from Source
###################

Fides uses `CMake <https://cmake.org/>`_ version 3.9 or above, for building,
testing, and installing the library and utilities.

************
Dependencies
************
* `ADIOS2 <https://github.com/ornladios/ADIOS2>`_: Most recent release is preferred, but most ADIOS2 versions should be fine.

* `VTK-m <https://gitlab.kitware.com/vtk/vtk-m>`_: Version 2.1 or later.

*****************************************
Building, Testing, and Installing Fides
*****************************************

To build Fides, clone the repository and invoke the canonical CMake build sequence:

.. code-block:: bash

    $ git clone https://gitlab.kitware.com/vtk/fides.git


If you want to run tests or use the test data, you'll need to download the data using Git LFS.

.. code-block:: bash

    $ cd fides
    $ git lfs install
    $ git lfs pull
    $ cd ..

Now continue with the build process:

.. code-block:: bash

    $ mkdir fides-build && cd fides-build
    $ cmake ../fides
    -- The C compiler identification is GNU 7.3.0
    -- The CXX compiler identification is GNU 7.3.0
    ...

Then compile using

.. code-block:: bash

    $ make

If you downloaded the test data, you can now optionally run the tests:

.. code-block:: bash

    $ ctest
    Test project /Users/caitlinross/dev/fides-build
          Start  1: test-basic
     1/17 Test  #1: test-basic ........................   Passed    0.20 sec
          Start  2: test-explicit
     2/17 Test  #2: test-explicit .....................   Passed    0.18 sec

          ...

          Start 17: validate-inline
    17/17 Test #17: validate-inline ...................   Passed    0.03 sec


And finally, use the standard invocation to install:

.. code-block:: bash

    $ make install

*************
CMake Options
*************

The following options can be specified with CMake's ``-DVAR=VALUE`` syntax. The default option is highlighted.

============================= ============================================== ========================================
VAR                            VALUE                                          Description
============================= ============================================== ========================================
``FIDES_ENABLE_EXAMPLES``       **ON**/OFF                                     Build examples
``FIDES_ENABLE_TESTING``        **ON**/OFF                                     Build tests
``BUILD_SHARED_LIBS``          **ON**/OFF                                     Build shared libraries.
``CMAKE_INSTALL_PREFIX``       /path/to/install (``/usr/local``)              Installation location.
``CMAKE_BUILD_TYPE``           Debug/**Release**/RelWithDebInfo/MinSizeRel    Compiler optimization levels.
============================= ============================================== ========================================
