### Documentation Build Instructions

The [user guide is hosted on ReadTheDoc's servers](https://fides.readthedocs.io/en/latest/) and is set to automatically update on merging MRs to Fides `master`.

If you'd like to build locally, you'll need to first install Doxygen and Sphinx.

Then set up the python environment and install requirements:

```
fides/docs$ python3 -m venv .
fides/docs$ . bin/activate
(docs)fides/docs$ pip3 install -r requirements.txt
```

Finally switch to your build directory for Fides.
When configuring CMake, set the option `-DFIDES_BUILD_DOCS=ON`, the rest of your build stays the same.
Then `make` as usual.
The Doxygen output may not be built before the Sphinx build occurs, so running `make` a second time may be necessary if you see any warnings/errors on the API page.

The Doxygen index page is located at `fides-build/docs/api_doxygen/html/index.html`, while the user guide index page is at `fides-build/docs/user_guide/index.html`.
