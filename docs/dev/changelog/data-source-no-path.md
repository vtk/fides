## Add ability to load data sources without providing a path

Previously, to load a JSON file with the data in a separate bp
file, you had to specify the directory or path of the data file
in the "paths" map provided to the `DataSetReader` read methods.
This is a problem because it makes the file formant not
self-descriptive. When loading data in an application like
ParaView, you have to type in the directory to the bp file
attached to a magic string defined somewhere in the JSON file.
Every. Damn. Time.

This change allows you to specify a relative path to the data
source .bp file within the JSON file. If you do not specify a
matching path, the reader will look for the .bp file relative to
the JSON file. If you do provide a path, then that path is used.
Thus, the old behavior is preserved.
