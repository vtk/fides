#!/bin/sh

# System cmake/ninja are required for building dependencies

# Install build requirements
apt-get update -y
apt-get install build-essential sudo curl \
                openmpi-bin libopenmpi-dev \
                ninja-build \
                git git-lfs vim \
                clang clang-tidy \
                -y
git lfs install
