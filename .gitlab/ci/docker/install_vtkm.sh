#!/bin/sh
set -x

set -x

readonly vtkm_repo="https://gitlab.kitware.com/vtk/vtk-m"
readonly vtkm_commit="v2.1.0"

readonly vtkm_root="$HOME/vtkm"
readonly vtkm_src="$vtkm_root/src"
readonly vtkm_build_root="$vtkm_root/build"

git clone -b "$vtkm_commit" "$vtkm_repo" "$vtkm_src"

cmake -GNinja \
    -S "$vtkm_src" \
    -B "$vtkm_build_root" \
    -DCMAKE_BUILD_TYPE=Release \
    -DBUILD_SHARED_LIBS=ON \
    -DBUILD_TESTING=OFF \
    -DCMAKE_INSTALL_PREFIX=/usr \
    -DCMAKE_INSTALL_LIBDIR=lib \
    -DVTKm_ENABLE_TESTING=OFF \
    -DVTKm_USE_DOUBLE_PRECISION=ON
cmake --build "$vtkm_build_root"
cmake --build "$vtkm_build_root" --target install

rm -rf "$vtkm_root"
